### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 63a0a4c0-5b3c-4250-b9a2-ec783b425c6c
using Pkg

# ╔═╡ c956ef20-a614-42b5-890d-c9123bacc7e5
Pkg.activate("Project.toml")

# ╔═╡ 09d52dd2-cc93-11ec-02a7-fd19b345aed8
using Markdown

# ╔═╡ e3986630-479b-4a11-8b02-090b6f0df48c
using InteractiveUtils

# ╔═╡ a47d66ff-a63d-41e2-9867-0a69add7ff3b
using PlutoUI

# ╔═╡ f8b685e6-44eb-4328-9d75-17560ad52a90
using DataStructures

# ╔═╡ 684112f1-eb9a-4c4e-8f2e-1bd590c082cf
using Printf

# ╔═╡ 1c95b2df-9d76-4d16-a997-2ffef0676399
@enum Domain a b c d e

# ╔═╡ 29fccdb5-c252-4176-aa79-ae6412746e92
md"### Creating Struct"

# ╔═╡ 2697940d-db2c-4a20-87a8-ac38b16ea7e6
struct Constant_Function_Dict{V}
    value::V
end

# ╔═╡ 5af45ae8-d782-4233-a0fe-c8c608fa4a2a
mutable struct CSPDictionary
    dict::Union{Nothing, Dict, Constant_Function_Dict}
end

# ╔═╡ 07778cfc-e0d3-4174-b541-a10d346a74cb
abstract type AbstractCSP
end

# ╔═╡ acb91b6b-f059-4c16-a972-aaa1ed31f93d
mutable struct CSP <: AbstractCSP
	vars::AbstractVector
	domains::CSPDictionary
	neighbors::CSPDictionary
	constraints::Function
	initial::Tuple
	current_domains::Union{Nothing, Dict}
	nassigns::Int64
end

# ╔═╡ cc85c090-19e6-4cc9-b8b6-f0d4c22a04f6
mutable struct CSPVar
	name::String
	value::Union{Nothing,Domain}
	forbidden_values::Vector{Domain}
	domain_count::Int64
end

# ╔═╡ fe3debca-aef1-47af-8c73-6a943af7c9f1
struct graph{T <: Real,U}
    edges::Dict{Tuple{U,U},T}
    verts::Set{U}
end

# ╔═╡ 1e7f977a-92cd-44ad-9d41-12c1a2b794a8
md"### Creating Methods"

# ╔═╡ 87303173-c505-415a-aae1-5142c149053b
function Graph(edges::Vector{Tuple{U,U,T}}) where {T <: Real,U}
    vnames = Set{U}(v for edge in edges for v in edge[1:2])
    adjmat = Dict((edge[1], edge[2]) => edge[3] for edge in edges)
    return graph(adjmat, vnames)
end

# ╔═╡ 4b786966-4a44-4b74-8508-d328c3ba8842
begin
	vertices(g::graph) = g.verts
	edges(g::graph)    = g.edges
end

# ╔═╡ dcbb9793-0324-4d29-9a54-e551418bfdfb
diff = rand(setdiff(Set([a,b,c,d]), Set([a,b])))

# ╔═╡ 40ea8faa-e66f-4caa-9851-7f6f2cdc6732
neighbours(g::graph, v) = Set((b, c) for ((a, b), c) in edges(g) if a == v)

# ╔═╡ 1ed7870c-f281-426b-aad6-86f2486de085
md"### Assign arguments"

# ╔═╡ b3d1ddb5-8f35-41bd-b9cf-1c986d1a6c20
function assigning(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
    assignment[key] = val;
    problem.nassigns = problem.nassigns + 1;
    nothing;
end

# ╔═╡ 3a27d34a-5ee9-4307-95b5-1204f421ec67
md"### Unassign arguments"

# ╔═╡ a060c5b6-bc15-4e03-a1e3-a4dc87b37936
function unassigning(problem::T, key, assignment::Dict) where {T <: AbstractCSP}
    if (haskey(assignment, key))
        delete!(assignment, key);
    end
    nothing;
end

# ╔═╡ 215f712e-2ba7-4968-87ad-42e8739bacad
function error(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
    return count(
                (function(second_key)
                    return (haskey(assignment, second_key) &&
                        !(problem.constraints(key, val, second_key, assignment[second_key])));
                end),
                problem.neighbors[key]);
end

# ╔═╡ a28da79b-bedf-4d23-9e62-f6d7ae8011ae
function display(problem::T, assignment::Dict) where {T <: AbstractCSP}
    println("Cons: ", problem, " with assignment: ", assignment);
    nothing;
end

# ╔═╡ d5313d78-5c7e-4ea4-ba04-efe516ac23cc
function actions(problem::T, state::Tuple) where {T <: AbstractCSP}
    if (length(state) == length(problem.vars))
        return [];
    else
        let
            local assignment = Dict(state);
            local var = problem.vars[findfirst((function(e)
                                        return !haskey(assignment, e);
                                    end), problem.vars)];
            return collect((var, val) for val in problem.domains[var]
                            if error(problem, var, val, assignment) == 0);
        end
    end
end

# ╔═╡ 644a3538-8cb5-41fb-b1ee-2eece3d646f8
function presult(problem::T, state::Tuple, action::Tuple) where {T <: AbstractCSP}
    return (state..., act);
end

# ╔═╡ 3e18e159-6f72-47c2-93bf-088c2423e2c3
md"### Path Finder"

# ╔═╡ ed5e767c-24b3-46ed-87e7-22f28a061f7f
function checkpath(g::graph{T,U}, source::U, dest::U) where {T, U}
    @assert source ∈ vertices(g) "$source is not a vertex in the graph"
 
    if source == dest return [source], 0 end
    # Initialize variables
    inf  = typemax(T)
    dist = Dict(v => inf for v in vertices(g))
    prev = Dict(v => v   for v in vertices(g))
    dist[source] = 0
    Q = copy(vertices(g))
    neigh = Dict(v => neighbours(g, v) for v in vertices(g))
 
    # Main loop
    while !isempty(Q)
        u = reduce((x, y) -> dist[x] < dist[y] ? x : y, Q)
        pop!(Q, u)
        if dist[u] == inf || u == dest break end
        for (v, cost) in neigh[u]
            alt = dist[u] + cost
            if alt < dist[v]
                dist[v] = alt
                prev[v] = u
            end
        end
    end
 
    # Return path
    rst, cost = U[], dist[dest]
    if prev[dest] == dest
        return rst, cost
    else
        while dest != source
            pushfirst!(rst, dest)
            dest = prev[dest]
        end
        pushfirst!(rst, dest)
        return rst, cost
    end
end

# ╔═╡ 3c01dd87-b7fc-4c95-8c70-04581dfbb4c6
md"### Goal test set"

# ╔═╡ 8289f5aa-1e24-4d03-a065-8741e4355ed3
function goalTest(problem::T, state::Dict) where {T <: AbstractCSP}
    let
        local assignment = deepcopy(state);
        return (length(assignment) == length(problem.vars) &&
                all((function(key)
                            return error(problem, key, assignment[key], assignment) == 0;
                        end),
                        problem.vars));
    end
end

# ╔═╡ 9b8e36fe-33e0-4ca6-97f2-f63cd28ca9f4
md"### cost set"

# ╔═╡ a07ac8e2-92a5-4831-9083-297e23741e2d
function path_cost(problem::T, cost::Float64, state1::Tuple, action::Tuple, state2::Tuple) where {T <: AbstractCSP}
    return cost + 1;
end

# ╔═╡ 790cb4e1-900d-49a9-85ad-7df0ef8d36ea
md"### Pruning method"

# ╔═╡ 8ce90a62-4c3b-4145-baec-b96b69eb2c76
function pruning(problem::T, key, value, removals) where {T <: AbstractCSP}
    local not_removed::Bool = true;
    for (i, element) in enumerate(problem.current_domains[key])
        if (element == value)
            deleteat!(problem.current_domains[key], i);
            not_removed = false;
            break;
        end
    end
    if (not_removed)
        error("Could not find ", value, " in ", problem.current_domains[key], " for key '", key, "' to be removed!");
    end
    if (!(typeof(removals) <: Nothing))
        push!(removals, Pair(key, value));
    end
    nothing;
end

# ╔═╡ c1f9d760-ff38-4df0-9d8c-1d3ab04057bc
function errorvariables(problem::T, current_assignment::Dict) where {T <: AbstractCSP}
    return collect(var for var in problem.vars
                    if (error(problem, var, current_assignment[var], current_assignment) > 0));
end

# ╔═╡ 938e38d4-996a-413b-84b6-c30a1fcf7147
md"### Forward check method"

# ╔═╡ 39003fbe-a183-4a08-962c-75167e555e1a
function forwardchecking(problem::T, var, value, assignment::Dict, removals::Union{Nothing, AbstractVector}) where {T <: AbstractCSP}
    for B in problem.neighbors[var]
        if (!haskey(assignment, B))
            for b in copy(problem.current_domains[B])
                if (!problem.constraints(var, value, B, b))
                    pruning(problem, B, b, removals);
                end
            end
            if (length(problem.current_domains[B]) == 0)
                return false;
            end
        end
    end
    return true;
end

# ╔═╡ 97a47cd1-a2b0-4b31-ac6c-fc8e561b0f06
md"### backtrack Method"

# ╔═╡ b5750e51-a0c5-4678-bada-402c3a3201d2
function backtrack(problem::T, assignment::Dict;
                    select_unassigned_variable::Function=first_unassigned_variable,
                    order_domain_values::Function=unordered_domain_values,
                    inference::Function=no_inference) where {T <: AbstractCSP}
    if (length(assignment) == length(problem.vars))
        return assignment;
    end
    local var = select_unassigned_variable(problem, assignment);
    for value in order_domain_values(problem, var, assignment)
        if (nconflicts(problem, var, value, assignment) == 0)
            assign(problem, var, value, assignment);
            removals = suppose(problem, var, value);
            if (inference(problem, var, value, assignment, removals))
                result = backtrack(problem, assignment,
                                    select_unassigned_variable=select_unassigned_variable,
                                    order_domain_values=order_domain_values,
                                    inference=inference);
                if (!(typeof(result) <: Nothing))
                    return result;
                end
            end
            reserve(problem, removals);
        end
    end
    unassign(problem, var, assignment);
    return nothing;
end

# ╔═╡ c6614145-fb90-4baf-b60b-a75d53c38999
function assignment(problem::T) where {T <: AbstractCSP}
    support(problem);
    return Dict(collect(Pair(key, problem.current_domains[key][1])
                        for key in problem.vars
                            if (1 == length(problem.current_domains[key]))));
end

# ╔═╡ 1a9ea561-182c-49f1-98ce-1b277636a896
testGraph = [("a", "b", 1), ("b", "e", 2), ("a", "e", 4)]

# ╔═╡ 75867177-5a5e-4b06-b375-747f6a2f4cd4
g = Graph(testGraph)

# ╔═╡ 60d1a316-6516-4cd1-b089-e89dded5ddff
src, dst = "a", "e"

# ╔═╡ bf6bd255-f76c-4184-8c0c-8583a0465dd4
path, cost = checkpath(g, src, dst)

# ╔═╡ 7b110fa2-750d-46d0-a52f-880bb8dc04db
with_terminal() do
	@printf("\n%3s | %2s | %s\n", "src", "dst", "path")
    @printf("-----------------\n")
	for src in vertices(g), dst in vertices(g)
    path, cost = checkpath(g, src, dst)
    @printf("%3s | %2s | %s\n", src, dst, isempty(path) ? "no possible path" : join(path, " → ") * " ($cost)")
	end
end

# ╔═╡ 6bd9f8bd-6f61-4f27-bdc7-ee4f794bea1e
with_terminal() do
	println("Shortest path from $src to $dst: ", isempty(path) ? "no possible path" : join(path, " → "), " (cost $cost)")
end

# ╔═╡ Cell order:
# ╠═09d52dd2-cc93-11ec-02a7-fd19b345aed8
# ╠═e3986630-479b-4a11-8b02-090b6f0df48c
# ╠═63a0a4c0-5b3c-4250-b9a2-ec783b425c6c
# ╠═c956ef20-a614-42b5-890d-c9123bacc7e5
# ╠═a47d66ff-a63d-41e2-9867-0a69add7ff3b
# ╠═f8b685e6-44eb-4328-9d75-17560ad52a90
# ╠═684112f1-eb9a-4c4e-8f2e-1bd590c082cf
# ╠═1c95b2df-9d76-4d16-a997-2ffef0676399
# ╠═29fccdb5-c252-4176-aa79-ae6412746e92
# ╠═2697940d-db2c-4a20-87a8-ac38b16ea7e6
# ╠═5af45ae8-d782-4233-a0fe-c8c608fa4a2a
# ╠═07778cfc-e0d3-4174-b541-a10d346a74cb
# ╠═acb91b6b-f059-4c16-a972-aaa1ed31f93d
# ╠═cc85c090-19e6-4cc9-b8b6-f0d4c22a04f6
# ╠═fe3debca-aef1-47af-8c73-6a943af7c9f1
# ╠═1e7f977a-92cd-44ad-9d41-12c1a2b794a8
# ╠═87303173-c505-415a-aae1-5142c149053b
# ╠═4b786966-4a44-4b74-8508-d328c3ba8842
# ╠═dcbb9793-0324-4d29-9a54-e551418bfdfb
# ╠═40ea8faa-e66f-4caa-9851-7f6f2cdc6732
# ╠═1ed7870c-f281-426b-aad6-86f2486de085
# ╠═b3d1ddb5-8f35-41bd-b9cf-1c986d1a6c20
# ╠═3a27d34a-5ee9-4307-95b5-1204f421ec67
# ╠═a060c5b6-bc15-4e03-a1e3-a4dc87b37936
# ╠═215f712e-2ba7-4968-87ad-42e8739bacad
# ╠═a28da79b-bedf-4d23-9e62-f6d7ae8011ae
# ╠═d5313d78-5c7e-4ea4-ba04-efe516ac23cc
# ╠═644a3538-8cb5-41fb-b1ee-2eece3d646f8
# ╠═3e18e159-6f72-47c2-93bf-088c2423e2c3
# ╠═ed5e767c-24b3-46ed-87e7-22f28a061f7f
# ╠═3c01dd87-b7fc-4c95-8c70-04581dfbb4c6
# ╠═8289f5aa-1e24-4d03-a065-8741e4355ed3
# ╠═9b8e36fe-33e0-4ca6-97f2-f63cd28ca9f4
# ╠═a07ac8e2-92a5-4831-9083-297e23741e2d
# ╠═790cb4e1-900d-49a9-85ad-7df0ef8d36ea
# ╠═8ce90a62-4c3b-4145-baec-b96b69eb2c76
# ╠═c1f9d760-ff38-4df0-9d8c-1d3ab04057bc
# ╠═938e38d4-996a-413b-84b6-c30a1fcf7147
# ╠═39003fbe-a183-4a08-962c-75167e555e1a
# ╠═97a47cd1-a2b0-4b31-ac6c-fc8e561b0f06
# ╠═b5750e51-a0c5-4678-bada-402c3a3201d2
# ╠═c6614145-fb90-4baf-b60b-a75d53c38999
# ╠═1a9ea561-182c-49f1-98ce-1b277636a896
# ╠═75867177-5a5e-4b06-b375-747f6a2f4cd4
# ╠═60d1a316-6516-4cd1-b089-e89dded5ddff
# ╠═bf6bd255-f76c-4184-8c0c-8583a0465dd4
# ╠═7b110fa2-750d-46d0-a52f-880bb8dc04db
# ╠═6bd9f8bd-6f61-4f27-bdc7-ee4f794bea1e
