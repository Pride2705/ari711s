### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 8b84773e-dfc4-4cd2-a0e2-127e2b94d696
using Pkg

# ╔═╡ 07e84454-529a-44d6-b1b7-5265298bee5f
Pkg.activate("Project.toml")

# ╔═╡ 1a513657-2f10-4562-bc62-0a2d493a4b53
Pkg.add("PlutoUI")

# ╔═╡ 8a95748f-a57c-460c-9e15-fd01b92edb77
Pkg.add("DataStructures")

# ╔═╡ 76f8ef30-cc8f-11ec-3cdb-dda246cf1e39
using Markdown

# ╔═╡ c1044220-bdc3-4310-898a-fdd04fa821f1
using InteractiveUtils

# ╔═╡ 25bd989c-5334-4f93-98cc-192e21dc4f1d
using PlutoUI

# ╔═╡ cc2d5268-3199-4e48-89fd-5789dd522f56
using DataStructures

# ╔═╡ 1e7bb359-073c-462f-bd5d-c7c293abd35e
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ 1ed35171-813a-4149-a7fe-1474e79b77f6
md"## State def "

# ╔═╡ b915d5cc-3bac-451d-b011-0bcb68851b3d
struct State
    name::String
    position::Int64
   parcels::Vector{Bool}
end

# ╔═╡ 88595017-42b1-4972-9600-6423ae5a81a5
md"## Action Def"

# ╔═╡ 13938ad8-4b76-4f79-aef0-c52140c5078c
struct Action
    name::String
    cost::Int64
end

# ╔═╡ 9028dbef-4095-466d-9a4e-20920e9ac116
md"## Assigning actions "

# ╔═╡ 277dbad1-7e16-4fa1-b728-7225d95e92c0
A1 = Action("me", 2)

# ╔═╡ ee64266a-0c9a-48f6-9003-dd4a7a889299
A2 = Action("mw", 2)

# ╔═╡ c1af4620-c1fe-43b0-9bd3-3a856184b1c1
A3 = Action("mu", 1)

# ╔═╡ 864b0939-e68d-482b-955f-cd424a7c6e59
A4 = Action("md", 1)

# ╔═╡ a29c5c7d-aafa-47ee-b3b4-099b6b1a077e
A5 = Action("co", 5)

# ╔═╡ d9aaffde-201b-49ce-9c28-7fb28a6c428a
md"## Assigning the states  "

# ╔═╡ dd5226db-d893-4668-9e87-2ddebe1c2292
S1 = State("Stateone", 1, [true, true, true])

# ╔═╡ d1faa692-b8e8-4821-ae79-921e10f7d9d4
S2 = State("Statetwo", 2, [true, true, true])

# ╔═╡ eaa21ecd-da60-4e37-b675-7700d8d74932
S3 = State("Statethree", 3, [true, true, true])

# ╔═╡ c81b0d8b-b05e-4f65-aa2c-32b02e868443
S4 = State("Statefour", 4, [true, true, true])

# ╔═╡ fe2a14d4-33e1-4aaf-8697-b12097ab781f
S5 = State("Statefive", 5, [true, true, true])

# ╔═╡ cac72d8b-67c2-4bd1-81a1-4cef0ed0bec2
S6 = State("Statesix", 1, [false, true, true])

# ╔═╡ d607b47f-fb9a-4253-9b0d-7a369442915f
S7 = State("Stateseven", 2, [false, true, true])

# ╔═╡ a71d9599-2178-49c3-8c58-7e94236e1c26
S8 = State("Stateeight", 3, [false, true, true])

# ╔═╡ c4d9fec0-02f0-42f1-87d4-c47703959f5c
S9 = State("Statenine", 4, [false, true, true])

# ╔═╡ ee4e9805-0e1a-429b-a40b-d4e864600e43
S10 = State("Stateten", 5, [false, false, false])

# ╔═╡ 8a2c1c01-4c9e-4ed5-80ff-d6290ddbdc60
md"### Transition model"

# ╔═╡ 921cc00c-6fcc-49d1-bab9-b4ecd9136cda
Transition_Model = Dict()

# ╔═╡ bb9c699e-eef8-4e02-8cda-237d08c2c57f
md"### Add a mapping"

# ╔═╡ 5eef6180-93c2-4133-88bf-1fa0b5c04ced
push!(Transition_Model, S1 => [(A2, S2), (A5, S6), (A3, S1)])

# ╔═╡ b6e8b0c7-4ccd-4942-ba36-daa10f743bd4
push!(Transition_Model, S1 => [(A2, S3), (A5, S7), (A1, S2)])

# ╔═╡ 0239c23c-7172-4d24-b944-cea499ab53e0
push!(Transition_Model, S1 => [(A4, S4), (A5, S8), (A1, S3)])

# ╔═╡ f6262538-348f-4aaa-bdb8-43abd8568d9f
push!(Transition_Model, S1 => [(A2, S5), (A5, S9), (A3, S4)])

# ╔═╡ da9399b2-1752-49f2-ac40-a00993150561
push!(Transition_Model, S10 => [(A2, S5), (A5, S9), (A4, S10)])

# ╔═╡ 81cb3500-d3b9-4bdc-8426-1f8df2ace059
Transition_Model

# ╔═╡ 8b360b2e-dea6-4350-a4ac-25cc7c3c8e69
md"### User Input"

# ╔═╡ 118a2290-25b8-469c-a891-934669498301
@bind storeys TextField()

# ╔═╡ 3c2eb93d-ba6d-473a-b8bf-554be77de830
with_terminal() do
	println(storeys)
end

# ╔═╡ b935aeab-dcae-4d92-9a10-81b992571c40
@bind offices_per_floor TextField()

# ╔═╡ 0fec5f3c-265b-4a0f-868d-5fadf6adf3b8
with_terminal() do
	println(offices_per_floor)
end

# ╔═╡ dc2bede4-3738-42cd-81c2-7deb948d74d2
@bind parcels_in_each_office TextField()

# ╔═╡ 0028c81e-8dc5-4625-9e3b-5dd9b8859ca7
with_terminal() do
	println(parcels_in_each_office)
end

# ╔═╡ 3f46b62d-5655-4262-b167-da023f5d8436
@bind location Radio(["S1", "S2", "S3"])

# ╔═╡ 1278f6f5-835d-40e6-9724-8275258e99e1
with_terminal() do
	println(location)
end

# ╔═╡ 203193e5-8ea2-46cb-81fe-c02294d4aa69
function create_search(Transition_Model, ancestors, initial_state, goal_state)
	result = []
	visitor = goal_state
	while !(visitor == initial_state)
		current_state_ancestor = ancestors[visitor]
		related_transitions = Transition_Model[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == visitor
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		visitor = current_state_ancestor
	end
	return result
end

# ╔═╡ ec84c19f-50dc-4321-9d5b-88416b77a160
md"### A star algorithm"

# ╔═╡ 3d7c0ee3-0386-4815-9b41-611654747dbb
function constructpath(S1, goal_state)
    return min(S1[1] - goal_state[1]) + min(S1[2] - goal_state[2])
end

# ╔═╡ 5d2235d7-8604-4094-90cb-b0302368ff4c
md"### Setting Goal test"

# ╔═╡ d7947575-351f-43c8-bf44-e0a09b9ffdfd
function goal_test(currentState::State)
    return ! (currentState.parcels[1] || currentState.parcels[3])
end

# ╔═╡ 6e0c887a-b4be-4027-bda0-c98bdce68929
with_terminal() do
	println( S10)
end

# ╔═╡ 77fffda8-c830-4085-941f-82980f5609d8
function strategy(initial_state, transition_dict, is_goal,all_candidates,
add_candidate, remove_candidate)
	visited = []
	ancestors = Dict{State, State}()
	the_candidates = add_candidate(all_candidates, initial_state, 0)
	parent = initial_state
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			currentState = t1

			push!(visited, currentState)
			candidates = transition_dict[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in visited)
					push!(ancestors, single_candidate[2] => currentState)
					if (is_goal(single_candidate[2]))
						return create_search(transition_dict, ancestors,
initial_state, single_candidate[2])
					else
						the_candidates = add_candidate(the_candidates,
single_candidate[2], single_candidate[1].cost)
						end
					end
				end
			end
		end
end

# ╔═╡ 5a02aa9b-b4da-43ba-9a77-1175db01fa34
function Astar(Transition_Model,initial_state, goal_state)
	
    result = []
	frontier = Queue{State}()
	explored = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initial_state)
	parent = initial_state
    while true
		if isempty(frontier)
			return []
		else
			currentState = dequeue!(frontier)
			push!(explored, currentState)
			candidates = Transition_Model[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return strategy(Transition_Model, parents,initial_state, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ 9ca24c16-a2b1-4cde-ad61-eab333e25dff
md"### Implementation of Moves"

# ╔═╡ 701a3041-c817-43b8-9ce1-61c4e9dc7905
function add_to_queue(queue::Queue{State}, state::State, cost::Int64)
    enqueue!(queue, state)
    return queue
end

# ╔═╡ 199ee0d2-3ab8-4a28-8569-d19609d4558c
function add_to_stack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ c431a135-724c-48c0-89a4-08dc08fb65c7
function remove_from_queue(queue::Queue{State})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ 624a612a-e744-42cd-9078-ee89d0fded63
strategy(S1, Transition_Model, goal_test, Queue{State}(), add_to_queue, remove_from_queue)

# ╔═╡ 1aebf97f-58e6-4571-bfe0-dbb3d1a45aa8
function remove_from_stack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end

# ╔═╡ deeca9b6-930f-4fde-b39a-7bc89d05a25f
function input(prompt::AbstractString)
    print(prompt)
    return readline()
end

# ╔═╡ Cell order:
# ╠═76f8ef30-cc8f-11ec-3cdb-dda246cf1e39
# ╠═c1044220-bdc3-4310-898a-fdd04fa821f1
# ╠═1e7bb359-073c-462f-bd5d-c7c293abd35e
# ╠═8b84773e-dfc4-4cd2-a0e2-127e2b94d696
# ╠═07e84454-529a-44d6-b1b7-5265298bee5f
# ╠═1a513657-2f10-4562-bc62-0a2d493a4b53
# ╠═8a95748f-a57c-460c-9e15-fd01b92edb77
# ╠═25bd989c-5334-4f93-98cc-192e21dc4f1d
# ╠═cc2d5268-3199-4e48-89fd-5789dd522f56
# ╠═1ed35171-813a-4149-a7fe-1474e79b77f6
# ╠═b915d5cc-3bac-451d-b011-0bcb68851b3d
# ╠═88595017-42b1-4972-9600-6423ae5a81a5
# ╠═13938ad8-4b76-4f79-aef0-c52140c5078c
# ╠═9028dbef-4095-466d-9a4e-20920e9ac116
# ╠═277dbad1-7e16-4fa1-b728-7225d95e92c0
# ╠═ee64266a-0c9a-48f6-9003-dd4a7a889299
# ╠═c1af4620-c1fe-43b0-9bd3-3a856184b1c1
# ╠═864b0939-e68d-482b-955f-cd424a7c6e59
# ╠═a29c5c7d-aafa-47ee-b3b4-099b6b1a077e
# ╠═d9aaffde-201b-49ce-9c28-7fb28a6c428a
# ╠═dd5226db-d893-4668-9e87-2ddebe1c2292
# ╠═d1faa692-b8e8-4821-ae79-921e10f7d9d4
# ╠═eaa21ecd-da60-4e37-b675-7700d8d74932
# ╠═c81b0d8b-b05e-4f65-aa2c-32b02e868443
# ╠═fe2a14d4-33e1-4aaf-8697-b12097ab781f
# ╠═cac72d8b-67c2-4bd1-81a1-4cef0ed0bec2
# ╠═d607b47f-fb9a-4253-9b0d-7a369442915f
# ╠═a71d9599-2178-49c3-8c58-7e94236e1c26
# ╠═c4d9fec0-02f0-42f1-87d4-c47703959f5c
# ╠═ee4e9805-0e1a-429b-a40b-d4e864600e43
# ╠═8a2c1c01-4c9e-4ed5-80ff-d6290ddbdc60
# ╠═921cc00c-6fcc-49d1-bab9-b4ecd9136cda
# ╠═bb9c699e-eef8-4e02-8cda-237d08c2c57f
# ╠═5eef6180-93c2-4133-88bf-1fa0b5c04ced
# ╠═b6e8b0c7-4ccd-4942-ba36-daa10f743bd4
# ╠═0239c23c-7172-4d24-b944-cea499ab53e0
# ╠═f6262538-348f-4aaa-bdb8-43abd8568d9f
# ╠═da9399b2-1752-49f2-ac40-a00993150561
# ╠═81cb3500-d3b9-4bdc-8426-1f8df2ace059
# ╠═8b360b2e-dea6-4350-a4ac-25cc7c3c8e69
# ╠═118a2290-25b8-469c-a891-934669498301
# ╠═3c2eb93d-ba6d-473a-b8bf-554be77de830
# ╠═b935aeab-dcae-4d92-9a10-81b992571c40
# ╠═0fec5f3c-265b-4a0f-868d-5fadf6adf3b8
# ╠═dc2bede4-3738-42cd-81c2-7deb948d74d2
# ╠═0028c81e-8dc5-4625-9e3b-5dd9b8859ca7
# ╠═3f46b62d-5655-4262-b167-da023f5d8436
# ╠═1278f6f5-835d-40e6-9724-8275258e99e1
# ╠═203193e5-8ea2-46cb-81fe-c02294d4aa69
# ╠═ec84c19f-50dc-4321-9d5b-88416b77a160
# ╠═3d7c0ee3-0386-4815-9b41-611654747dbb
# ╠═5d2235d7-8604-4094-90cb-b0302368ff4c
# ╠═d7947575-351f-43c8-bf44-e0a09b9ffdfd
# ╠═6e0c887a-b4be-4027-bda0-c98bdce68929
# ╠═77fffda8-c830-4085-941f-82980f5609d8
# ╠═5a02aa9b-b4da-43ba-9a77-1175db01fa34
# ╠═9ca24c16-a2b1-4cde-ad61-eab333e25dff
# ╠═701a3041-c817-43b8-9ce1-61c4e9dc7905
# ╠═199ee0d2-3ab8-4a28-8569-d19609d4558c
# ╠═c431a135-724c-48c0-89a4-08dc08fb65c7
# ╠═624a612a-e744-42cd-9078-ee89d0fded63
# ╠═1aebf97f-58e6-4571-bfe0-dbb3d1a45aa8
# ╠═deeca9b6-930f-4fde-b39a-7bc89d05a25f
