### A Pluto.jl notebook ###
# v0.18.1

using Markdown
using InteractiveUtils

# ╔═╡ 7d2540b0-7876-4692-b7aa-967defb3e985
using Pkg

# ╔═╡ 7a02bf35-6013-4e82-ab17-e031e8856d8b
Pkg.activate("Project.toml")

# ╔═╡ a6095b06-d376-4a47-95f4-b41d03fd064b
Pkg.add("MLJFlux")

# ╔═╡ 440923b7-9228-4b6b-8661-180e3f8c51b0
Pkg.add("Plots")

# ╔═╡ a9b6bf76-a945-433c-a0bb-df0266c02352
Pkg.add("Images")

# ╔═╡ cb0387be-36b6-43bf-94e3-95fe78c550bf
Pkg.add("ImageFeatures")

# ╔═╡ 69e87e62-3fae-422b-af36-cb014c636ca9
Pkg.add("ImageView")

# ╔═╡ 0bf1cc2e-983e-4cfb-9f6e-ee67dd6ca940
Pkg.add("BSON")

# ╔═╡ 0bdcc8dc-02c7-4be2-85a4-6800b0e3828e
Pkg.add("Parameters")

# ╔═╡ 87c52402-8aaf-417b-b1e3-96c00853592b
Pkg.add("JpegTurbo")

# ╔═╡ 747132d8-b44e-11ec-1e95-9ba8e55be24d
using Markdown

# ╔═╡ bee7bf2f-4745-420a-bcc8-d303c9addd3d
using InteractiveUtils

# ╔═╡ b4064340-b1c2-43cb-a4a1-9915bd8ecfcb
using Images

# ╔═╡ f715e3f4-2f29-400f-8d71-aa2fb01d63aa
using ImageView

# ╔═╡ 93d000e6-6f06-4942-8973-d8552e19d62f
using TestImages

# ╔═╡ 444082b5-7d98-4a8a-8fae-8a15bd8dfa3d
using FileIO

# ╔═╡ 2ae9edfd-0db7-4606-bed0-fd841b25320e
using PlutoUI

# ╔═╡ 90701d04-aa8f-49b9-a033-020eee6b02ec
using Flux

# ╔═╡ 2aa82648-3e52-4143-9cb7-fc6941cd4ca4
using Flux: Data.DataLoader

# ╔═╡ bc7c2461-49d4-45b7-aad5-9260647da725
using Flux: gradient

# ╔═╡ dbb8d40b-7164-4bf8-9afc-0c843a164583
using Flux: @epochs

# ╔═╡ fd0c512f-bdca-42f8-bf4b-aecaff20a8b7
using Statistics

# ╔═╡ dc77eb05-947b-4755-8473-6af838585f74
using Flux: onehotbatch, onecold, crossentropy, throttle, logitcrossentropy

# ╔═╡ 06a6a5a2-c5b3-45b3-85e0-d34710fe5d23
using Base.Iterators: repeated, partition

# ╔═╡ 12ee3c88-bdae-4cc3-8120-51caab56a281
using BSON

# ╔═╡ f0a56347-101c-4061-8541-47b42d8a90c8
using Printf

# ╔═╡ 28e4774a-dc21-4dec-a3e6-cdeb08a4d700
using Parameters: @with_kw

# ╔═╡ 368bb7e8-65b9-41c0-8e07-78559b7fbd0c
Pkg.add("TestImages")

# ╔═╡ f7a1b591-79d3-49de-b70e-977161dcdf45
Pkg.add("MLDataUtils")

# ╔═╡ 74c023a6-d0d5-45db-8b80-247ee50ac7b8
Pkg.add("MLDatasets")

# ╔═╡ e273264c-beab-454f-a7ef-371e958c0173
Pkg.add("CSV")

# ╔═╡ bd4c4ee3-070b-40d5-8f00-4fc7357bbd04
Pkg.add("FileIO")

# ╔═╡ 820585f1-3495-4044-926e-1aad3c0b0eb1
Pkg.add("Flux")

# ╔═╡ aa510f60-580d-49ed-93c9-ea99c62005d9
Pkg.add("PlutoUI")

# ╔═╡ ed58199f-1da9-44b5-b899-fe47bcd3f4de
Pkg.add("Statistics")

# ╔═╡ 690d3508-270e-4c22-8b47-1e0400e99951
Pkg.add("Plots")

# ╔═╡ db3cec4e-6988-433a-b8a8-673605312bd0
function load_images(path::String)
    img_paths = readdir(path)
    imgs = []
    for img_path in img_paths
        push!(imgs, load(string(path,img_path)))
    end
    return imgs
end

# ╔═╡ 05a1a325-d525-49a8-9fbb-f042c4017bc5
test_normal = [load(img) for img in readdir("chest_xray/test/NORMAL", join = true)]

# ╔═╡ 490ca87f-e623-4fdf-a897-a2f239e4a7df
test_2imgs = [load(img) for img in readdir("chest_xray/test/PNEUMONIA", join = true)]

# ╔═╡ b157d7ef-967b-419d-a8c0-4c7cdf37e8ba
train_1imgs = [load(img) for img in readdir("chest_xray/train/NORMAL", join = true)]

# ╔═╡ 4886402f-ef59-4223-9faf-61ffafcda61b
tn_img = load("chest_xray/train/NORMAL/IM-0115-0001.jpeg")

# ╔═╡ 0e154fce-f323-4a03-b952-569069c4056f
train_2imgs = [load(img) for img in readdir("chest_xray/train/PNEUMONIA", join = true)]

# ╔═╡ a131be1e-2873-4d4d-a1b1-a22fa7862f35
tp_img = load("chest_xray/train/PNEUMONIA/person1_bacteria_1.jpeg")

# ╔═╡ a25e6a1f-4f42-4346-8fa0-d9bbd7eda723
val_normal = [load(img) for img in readdir("chest_xray/val/NORMAL", join = true)]

# ╔═╡ 28a31155-660a-4e5f-82ba-5f86076b8173
val_peunomia = [load(img) for img in readdir("chest_xray/val/PNEUMONIA", join = true)]

# ╔═╡ 2037ba16-61d5-4270-9532-92df2894de4c
size(test_1imgs)

# ╔═╡ b6b3fecc-e6ca-4b11-8c85-df5157a7ffaf
size(test_2imgs)

# ╔═╡ 22dc90b7-cdf6-466c-baee-1f9a64832a11
size(train_1imgs)

# ╔═╡ c67f3d21-655c-4eed-ae1e-015fe1e6ed40
size(train_2imgs)

# ╔═╡ d978a16b-a2ac-4d43-8098-64faf08862ab
size(val_1imgs)

# ╔═╡ 1aa9cf8f-7ad0-4fee-9ee9-7ac906ddc50c
size(val_2imgs)

# ╔═╡ 2c4fb8c6-2d45-49f7-aa04-befed36d9e55
df(5)

# ╔═╡ 2b768728-67e8-4291-a554-a27fbf9cdd01
myloss(W, b, x) = sum(W * x .+ b)

# ╔═╡ eaed3a2b-78bf-4708-a89a-a0087231090a
m = Dense(10, 5)

# ╔═╡ 9fe70d98-7fd8-428b-9e80-dbb4f706e124
function make_batch(X, Y, idxs)
    Xbatch = Array{Float32}(undef, size(X[1])..., 1, length(idxs))
    for i in 1:length(idxs)
        Xbatch[:, :, :, i] = Float32.(X[idxs[i]])
    end
	
    Ybatch = onehotbatch(Y[idxs], 0:9)
    return (Xbatch, Ybatch)
end

# ╔═╡ 98ce1b3b-10e8-484b-9089-784e5dcc7c96
Array{Tuple{Array{Float32,4},Flux.OneHotMatrix{Array{Flux.OneHotVector,1}}},1}

# ╔═╡ 1254e227-dabd-44ab-b8d6-1d7954d828e8
model = Chain(
    # First convolution
    Conv((3, 3), 1=>16, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Second convolution
    Conv((3, 3), 16=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    # Third convolution
    Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
	
	Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),
	
	Conv((3, 3), 32=>32, pad=(1,1), relu),
    x -> maxpool(x, (2,2)),

    x -> reshape(x, :, size(x, 4)),
    Dense(288, 10),

    softmax,
)

# ╔═╡ efdc6d20-2393-44f6-b495-8778e8e99a47
function loss(x, y)
    x_aug = x .+ 0.1f0*gpu(randn(eltype(x), size(x)))

    y_hat = model(x_aug)
    return crossentropy(y_hat, y)
end

# ╔═╡ aedea29d-4670-4d9e-8f81-8e12f694ae9c
accuracy(x, y) = mean(onecold(model(x)) .== onecold(y))

# ╔═╡ e7ca1cf3-f5d9-49b0-9556-fecb5a8199d3
opt = ADAM(0.001)

# ╔═╡ 9ff54959-2978-4ad5-8d4f-ec19aeb8bf52
begin
	augment(x) = x .+ gpu(0.1f0*randn(eltype(x), size(x)))
	
	# Returns a vector of all parameters used in model
    paramvec(m) = hcat(map(p->reshape(p, :), params(m))...)

    # Function to check if any element is NaN or not
    anynan(x) = any(isnan.(x))

    accuracy(x, y, model) = mean(onecold(cpu(model(x))) .== onecold(cpu(y)))
end

# ╔═╡ dc4cf652-4ea9-48ff-9db0-51d0a5c89439
function sigmoid(z)
1 ./ (1 .+ exp.(.-z))
end

# ╔═╡ 6ea0095b-6900-4d6b-809c-2c3f383508df
function transform_features(xMatrix, u, o)
       XNorm = (xMatrix .- u) ./ o
       return X_norm
       end

# ╔═╡ e797b6aa-c966-48ce-bd28-52ff5c950219
begin
	W = randn(3, 5)
    b = zeros(3)
    x = rand(5)
end

# ╔═╡ 95225b6d-b433-4511-b1e0-7d32c3aae27b
gradient(myloss, W, b, x)

# ╔═╡ 8d6e0a34-e6e8-495e-85da-33b3d9630e49
size(train_set[1][1]) # training data Float32

# ╔═╡ cc7b846a-eb3a-4af8-a818-57bf0ddf2820
size(train_set[1][2]) # OneHotVector labels

# ╔═╡ b7e34fa8-310b-4f69-8bc8-943a88d92b48
train_set[1][2][:,1]


# ╔═╡ 92ffdf44-7f0e-43fc-99c4-891207962b31
model(train_set[1][1])

# ╔═╡ 9cc4518a-1b16-4794-bd4a-bc5aefce7440
begin
	best_acc = 0.0
    last_improvement = 0
    for epoch_idx in 1:100
		global best_acc, last_improvement
        # Train for a single epoch
        Flux.train!(loss, params(model), train_set, opt)

        # Calculate accuracy:
        acc = accuracy(test_set...)
    
        # If our accuracy is good enough, quit out.
        if acc >= 0.999
     
      end

       # If this is the best accuracy we've seen so far, save the model out
        if acc >= best_acc
        @info(" -> New best accuracy! Saving model out to mnist_conv.bson")
        BSON.@save "mnist_conv.bson" model epoch_idx acc
        best_acc = acc
        last_improvement = epoch_idx
        end

        if epoch_idx - last_improvement >= 5 && opt.eta > 1e-6
			opt.eta /= 10.0
     
        last_improvement = epoch_idx
        end

        if epoch_idx - last_improvement >= 10
        
		end
    end
end

# ╔═╡ 774c4195-fbda-47fe-b117-d9d00de01dbc

begin
	test_set = make_minibatch(test_imgs, test_labels, 1:length(test_imgs))
	typeof(train_set)
end

# ╔═╡ 99c2ea46-118f-4091-81dd-08489ceed4e7
begin
	batch_size = 128
    mb_idxs = partition(1:length(train_imgs), batch_size)
    train_set = [make_minibatch(train_imgs, train_labels, i) for i in mb_idxs]
end


# ╔═╡ f1687969-c603-4e41-8886-bd73231657e0
begin
	train_set = gpu.(train_set)
    test_set = gpu.(test_set)
    modl = gpu(model)
end

# ╔═╡ Cell order:
# ╠═747132d8-b44e-11ec-1e95-9ba8e55be24d
# ╠═bee7bf2f-4745-420a-bcc8-d303c9addd3d
# ╠═7d2540b0-7876-4692-b7aa-967defb3e985
# ╠═7a02bf35-6013-4e82-ab17-e031e8856d8b
# ╠═a6095b06-d376-4a47-95f4-b41d03fd064b
# ╠═440923b7-9228-4b6b-8661-180e3f8c51b0
# ╠═a9b6bf76-a945-433c-a0bb-df0266c02352
# ╠═cb0387be-36b6-43bf-94e3-95fe78c550bf
# ╠═69e87e62-3fae-422b-af36-cb014c636ca9
# ╠═368bb7e8-65b9-41c0-8e07-78559b7fbd0c
# ╠═f7a1b591-79d3-49de-b70e-977161dcdf45
# ╠═74c023a6-d0d5-45db-8b80-247ee50ac7b8
# ╠═e273264c-beab-454f-a7ef-371e958c0173
# ╠═bd4c4ee3-070b-40d5-8f00-4fc7357bbd04
# ╠═820585f1-3495-4044-926e-1aad3c0b0eb1
# ╠═aa510f60-580d-49ed-93c9-ea99c62005d9
# ╠═ed58199f-1da9-44b5-b899-fe47bcd3f4de
# ╠═690d3508-270e-4c22-8b47-1e0400e99951
# ╠═0bf1cc2e-983e-4cfb-9f6e-ee67dd6ca940
# ╠═0bdcc8dc-02c7-4be2-85a4-6800b0e3828e
# ╠═87c52402-8aaf-417b-b1e3-96c00853592b
# ╠═b4064340-b1c2-43cb-a4a1-9915bd8ecfcb
# ╠═f715e3f4-2f29-400f-8d71-aa2fb01d63aa
# ╠═93d000e6-6f06-4942-8973-d8552e19d62f
# ╠═444082b5-7d98-4a8a-8fae-8a15bd8dfa3d
# ╠═2ae9edfd-0db7-4606-bed0-fd841b25320e
# ╠═90701d04-aa8f-49b9-a033-020eee6b02ec
# ╠═2aa82648-3e52-4143-9cb7-fc6941cd4ca4
# ╠═bc7c2461-49d4-45b7-aad5-9260647da725
# ╠═dbb8d40b-7164-4bf8-9afc-0c843a164583
# ╠═fd0c512f-bdca-42f8-bf4b-aecaff20a8b7
# ╠═dc77eb05-947b-4755-8473-6af838585f74
# ╠═06a6a5a2-c5b3-45b3-85e0-d34710fe5d23
# ╠═12ee3c88-bdae-4cc3-8120-51caab56a281
# ╠═f0a56347-101c-4061-8541-47b42d8a90c8
# ╠═28e4774a-dc21-4dec-a3e6-cdeb08a4d700
# ╠═db3cec4e-6988-433a-b8a8-673605312bd0
# ╠═05a1a325-d525-49a8-9fbb-f042c4017bc5
# ╠═490ca87f-e623-4fdf-a897-a2f239e4a7df
# ╠═b157d7ef-967b-419d-a8c0-4c7cdf37e8ba
# ╠═4886402f-ef59-4223-9faf-61ffafcda61b
# ╠═0e154fce-f323-4a03-b952-569069c4056f
# ╠═a131be1e-2873-4d4d-a1b1-a22fa7862f35
# ╠═a25e6a1f-4f42-4346-8fa0-d9bbd7eda723
# ╠═28a31155-660a-4e5f-82ba-5f86076b8173
# ╠═2037ba16-61d5-4270-9532-92df2894de4c
# ╠═b6b3fecc-e6ca-4b11-8c85-df5157a7ffaf
# ╠═22dc90b7-cdf6-466c-baee-1f9a64832a11
# ╠═c67f3d21-655c-4eed-ae1e-015fe1e6ed40
# ╠═d978a16b-a2ac-4d43-8098-64faf08862ab
# ╠═1aa9cf8f-7ad0-4fee-9ee9-7ac906ddc50c
# ╠═2c4fb8c6-2d45-49f7-aa04-befed36d9e55
# ╠═2b768728-67e8-4291-a554-a27fbf9cdd01
# ╠═eaed3a2b-78bf-4708-a89a-a0087231090a
# ╠═9fe70d98-7fd8-428b-9e80-dbb4f706e124
# ╠═98ce1b3b-10e8-484b-9089-784e5dcc7c96
# ╠═1254e227-dabd-44ab-b8d6-1d7954d828e8
# ╠═8d6e0a34-e6e8-495e-85da-33b3d9630e49
# ╠═cc7b846a-eb3a-4af8-a818-57bf0ddf2820
# ╠═b7e34fa8-310b-4f69-8bc8-943a88d92b48
# ╠═92ffdf44-7f0e-43fc-99c4-891207962b31
# ╠═efdc6d20-2393-44f6-b495-8778e8e99a47
# ╠═aedea29d-4670-4d9e-8f81-8e12f694ae9c
# ╠═e7ca1cf3-f5d9-49b0-9556-fecb5a8199d3
# ╠═9ff54959-2978-4ad5-8d4f-ec19aeb8bf52
# ╠═9cc4518a-1b16-4794-bd4a-bc5aefce7440
# ╠═dc4cf652-4ea9-48ff-9db0-51d0a5c89439
# ╠═6ea0095b-6900-4d6b-809c-2c3f383508df
# ╠═e797b6aa-c966-48ce-bd28-52ff5c950219
# ╠═95225b6d-b433-4511-b1e0-7d32c3aae27b
# ╠═f1687969-c603-4e41-8886-bd73231657e0
# ╠═99c2ea46-118f-4091-81dd-08489ceed4e7
# ╠═774c4195-fbda-47fe-b117-d9d00de01dbc
